# schneebelietal\_biasd\_sensory\_uncertainty\_task #

This repository contains the analysis plans and code review for the manuscript: *Disentangling “Bayesian brain” theories of autism spectrum disorder* from M. Schneebeli, H. Haker, S. Iglesias, A. Rüesch, N. Zahnd, S. Marino, G. Paolini, F. H. Petzschner and  K. E. Stephan

It contains the following files:

* **Analysisplan\_SUT.docx**: This is the original time-stamped analysis plan and contains few comments and track changes
* **Analysisplan\_SUT\_v1.docx**: This file is equivalent to the original analysis plan, but clean version
* **Analysisplan\_SUT\_v2.docx**: This file contains the analysis plan with some major changes. All changes and reason for changes are summarized in the revision section. 
* **Analysisplan\_SUT\_v3.docx**: This file contains the analysis restructured and renumbered with concern to the manuscript. For better understanding, hypotheses were differently presented than in the original version of the analysis plans. More details are given in the revision section.
* **Code\_Review\_SUT.docx** Finally, a documentation of the code review of A. Rüesch is given. 
